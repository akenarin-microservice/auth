const express = require('express');
const router = express.Router();
const argon2 = require('argon2');
const { QueryTypes } = require('sequelize');
const jwt = require('jsonwebtoken');

const model = require('../models/index');
const passportJWT = require('../middlewares/passport-jwt');

//get profile (คนที่กำลังล็อกอินอยู่)
/* /api/v1/users/profile */
router.get('/profile', [passportJWT.isLogin] , async function(req, res, next) {
  const user = await model.User.findByPk(req.user.user_id);

  return res.status(200).json({
    user: {
      id: user.id,
      fullname: user.fullname,
      email: user.email,
      created_at: user.created_at
    }
  });
});

/* /api/v1/users/ */
router.get('/', async function(req, res, next) {
  // const users = await model.User.findAll();
  // const users = await model.User.findAll({
  //   // attributes: ['id', 'fullname']
  //   attributes: { exclude: ['password'] },
  //   order: [['id', 'desc']]
  // });

  const sql = 'select id,fullname,email from `users` order by id desc';
  const users = await model.sequelize.query(sql, { 
    type: QueryTypes.SELECT 
  });

  const totalUsers = await model.User.count();

  return res.status(200).json({
    total: totalUsers,
    data: users
  });
});

/* /api/v1/users/register */
router.post('/register', async function(req, res, next) {
  const {fullname, email, password} = req.body;

  //1. เช็คอีเมล์ซ้ำ
  const user = await model.User.findOne({ where: { email: email } });
  if (user !== null) {
    return res.status(400).json({message: 'มีผู้ใช้งานอีเมล์นี้แล้ว'});
  }

  //2. เข้ารหัสรหัสผ่าน
  const passwordHash = await argon2.hash(password);

  //3. บันทึกลงตาราง users
  const newUser = await model.User.create({
    fullname: fullname,
    email: email,
    password: passwordHash
  });

  return res.status(201).json({
    message: 'ลงทะเบียนสำเร็จ',
    user: {
      id: newUser.id,
      fullname: newUser.fullname
    },
  });
});

// Login
/* /api/v1/users/login */
router.post('/login', async function(req, res, next) {
  const { email, password } = req.body;

  //1. เช็คอีเมล์ว่ามีในระบบหรือไม่
  const user = await model.User.findOne({ where: { email: email } });
  if (user === null) {
    return res.status(404).json({message: 'ไม่พบผู้ใช้นี้ในระบบ'});
  }

  //2. นำรหัสผ่าน มาเปรียบเทียบกับรหัสผ่านในตารางฐานข้อมูล
  const isValid = await argon2.verify(user.password, password);
  if (!isValid) {
    return res.status(401).json({message: 'รหัสผ่านไม่ถูกต้อง'});
  }

  //3. สร้าง token
  const token = jwt.sign({ user_id: user.id }, process.env.JWT_KEY, { expiresIn: '7d' });

  return res.status(200).json({
    message: 'เข้าระบบสำเร็จ',
    access_token: token
  });
});

module.exports = router;
